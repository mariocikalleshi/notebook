import { useState, useEffect } from "react";
import "./App.css";
import FullNote from "./FullNote";
import NotesList from "./NotesList";
import apiRequest from "./apiRequest";

const App = () => {
  const [notes, setNotes] = useState([]);
  const [isPending, setIsPending] = useState(true);
  const [activeNote, setActiveNote] = useState(false);
  const [fetchError, setFetchError] = useState(null);

  const addNotes = async () => {
    const id = notes.length ? notes[notes.length - 1].id + 1 : 1;
    const newNote = {
      id,
      updated: new Date(),
      title: "Untitled",
      body: "",
      tags: [],
    };
    setNotes([...notes, newNote]);
    const postOption = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newNote),
    };

    const res = await apiRequest("http://localhost:8000/notes/", postOption);
    if (res) setFetchError(res);
  };

  const getNotes = async () => {
    try {
      const res = await fetch("http://localhost:8000/notes/");
      if (!res.ok) throw Error("Wrong response");
      const data = await res.json();
      setNotes(data);
      setIsPending(false);
      setFetchError(null);
    } catch (error) {
      setFetchError(error.message);
    }
  };

  useEffect(() => {
    getNotes();
  }, []);

  const deleteNote = async (noteId) => {
    await fetch(`http://localhost:8000/notes/${noteId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(notes),
    });
    getNotes();
  };

  const getActiveNote = () => {
    return notes.find((key) => key.id === activeNote);
  };

  const onUpdateNote = async (updatedNote) => {
    const listNotes = notes.map((note) => {
      if (note.id === updatedNote.id) {
        note = {
          ...note,
          updated: new Date(),
          title: updatedNote.title,
          body: updatedNote.body,
        };
        note.tags = updatedNote.tags;

        return note;
      } else return note;
    });
    setNotes(listNotes);
    const myNote = listNotes.filter((note) => note.id === updatedNote.id);

    await fetch(`http://localhost:8000/notes/${updatedNote.id}`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: myNote[0].title,
        body: myNote[0].body,
        tags: myNote[0].tags,
        updated: myNote[0].updated,
      }),
    });
  };

  return (
    <div className="App">
      {isPending && <div>Loading...</div>}

      {fetchError && <p style={{ color: "red" }}>{`Error: ${fetchError}`}</p>}
      {notes && (
        <NotesList
          notes={notes}
          addNotes={addNotes}
          deleteNote={deleteNote}
          activeNote={activeNote}
          setActiveNote={setActiveNote}
        />
      )}

      {!fetchError && (
        <FullNote activeNote={getActiveNote()} onUpdateNote={onUpdateNote} />
      )}
    </div>
  );
};

export default App;
