import { useState } from "react";
const NotesList = ({
  notes,
  addNotes,
  deleteNote,
  activeNote,
  setActiveNote,
}) => {
  const [search, setSearch] = useState("");

  const handleChange = (e) => {
    e.preventDefault();
    setSearch(e.target.value);
  };

  if (search.length > 0) {
    notes = notes.filter((i) => {
      const match = i.tags.find((element) => {
        if (element.match(search.toLowerCase())) {
          return true;
        }
      });

      return (
        match ||
        i.title.toLowerCase().match(search.toLowerCase()) ||
        i.body.toLowerCase().match(search.toLowerCase())
      );
    });
  }

  return (
    <div className="app-notes-list">
      <div className="app-notes-list-search">
        <input
          type="text"
          id="search"
          placeholder="Search titles, tags or text..."
          onChange={handleChange}
        />
      </div>
      <div className="app-notes-list-header">
        <h1>All Notes</h1>
        <button onClick={() => addNotes()}>Add</button>
      </div>

      <div className="app-notes-list-notes">
        {notes.map((note) => (
          <div
            className={`app-notes-list-note ${
              note.id === activeNote && "active"
            }`}
            key={note.id}
            onClick={() => setActiveNote(note.id)}
          >
            <div className="app-notes-list-title">
              <strong>{note.title}</strong>
              <button onClick={(e) => deleteNote(note.id)}>Delete</button>
            </div>

            <p>{note.body && note.body.substr(0, 100) + "..."}</p>
            <small className="note-meta">
              Last Modified{" "}
              {new Date(note.updated).toLocaleDateString("en-GB", {
                hour: "2-digit",
                minute: "2-digit",
              })}
            </small>
          </div>
        ))}
      </div>
    </div>
  );
};

export default NotesList;
