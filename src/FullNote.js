import { useState } from "react";
function FullNote({ activeNote, onUpdateNote }) {
  const [tags, setTags] = useState([]);
  if (!activeNote) return <div className="no-active-note">No Active Note</div>;

  const onEditField = (title, value, values) => {
    onUpdateNote({
      ...activeNote,
      [title]: value,
    });
  };

  const addTag = (e) => {
    if (e.key === "Enter" || e.key === " ") {
      if (e.target.value.length > 0) {
        activeNote.tags.push(e.target.value);
        e.target.value = "";
      }

      onUpdateNote(activeNote);
    }
  };

  const removeTag = (tag) => {
    const newTag = activeNote.tags.filter((a) => a !== tag);
    setTags(newTag);
    activeNote.tags = newTag;

    onUpdateNote(activeNote);
  };

  return (
    <div className="app-full-note">
      <div className="app-full-note-edit">
        <form>
          <input
            type="text"
            id="title"
            value={activeNote.title}
            onChange={(e) => onEditField("title", e.target.value)}
            autoFocus
          />
          <div className="tag-container" >
            {activeNote.tags.map((tag, index) => {
              return (
                <div key={index} className="tag">
                  {tag && tag} <span onClick={() => removeTag(tag)}>x</span>
                </div>
              );
            })}

            <input placeholder="Press enter or space to add tags" onKeyDown={(e) => addTag(e)} />
          </div>

          <textarea
            id="body"
            placeholder="Write your notes here..."
            value={activeNote.body}
            onChange={(e) => onEditField("body", e.target.value)}
          />
        </form>
      </div>

      <div className="app-full-note-preview">
        <h1 className="preview-title">{activeNote.title}</h1>
        <div className="markdown-preview">{activeNote.body}</div>
      </div>
    </div>
  );
}

export default FullNote;
